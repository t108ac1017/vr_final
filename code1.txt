using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject BubblePrefab;
    public BoxCollider spawnArea;//不用拉東西進去; 但是GameManager要加Boxcollider component
    Vector3 maxSpawnPos;
    public VideoPlayer vp;
    float lastSpawnTimeS = -1;
    public float spawnDelayS = 5; //物件生成間隔
    public GameObject start_bubble, start, spawn, gameover;

// Use this for initialization
void Start()
{
        Instantiate(start_bubble, new Vector3(0, 4, 12), transform.rotation);
        start = GameObject.Find("start_bubble(Clone)");
        vp = GameObject.Find("Canvas/Countdown/RawImage").GetComponent<VideoPlayer>();
        vp.loopPointReached += EndReached;
    spawnArea = GetComponent<BoxCollider>();
    spawnArea.enabled = false; 
    maxSpawnPos = new Vector3(spawnArea.size.x / 2, spawnArea.size.y / 2, spawnArea.size.z / 2);
}

// Update is called once per frame
void Update()
{
        if(!start)
        {
            vp.Play();
            if (lastSpawnTimeS < 0)
            {
                lastSpawnTimeS = Time.time;
                GameObject nextBubble = Instantiate(BubblePrefab, Vector3.zero, Quaternion.identity) as GameObject;
                nextBubble.transform.parent = transform;
                Vector3 pos = new Vector3(Random.Range(-maxSpawnPos.x, maxSpawnPos.x) % spawnArea.size.x, Random.Range(-maxSpawnPos.y, maxSpawnPos.y) % spawnArea.size.y, Random.Range(-maxSpawnPos.z, maxSpawnPos.z) % spawnArea.size.z);
                nextBubble.transform.localPosition = pos;
            }
            else if (lastSpawnTimeS >= 0 && Time.time - lastSpawnTimeS > spawnDelayS)
            {
                lastSpawnTimeS = -1;
            }
        }
        
}
    void EndReached(VideoPlayer vp)
    {
        spawn.SetActive(false);
        gameover.SetActive(true);
        Instantiate(start_bubble, new Vector3(0, 4, 12), transform.rotation);
        start = GameObject.Find("start_bubble(Clone)");
    }
}